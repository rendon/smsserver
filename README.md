SMSServer
=========
A little HTTP server for sending and receiving SMS's.

Maybe not the best way to create a server for SMS, but it's my first Android
Application. I think it's not bad to start.

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=833](http://rendon.x10.mx/?p=833).

License
=======
All the code, with exception of NanoHTTPD which has its own license, is under
GNU GPLv3.

