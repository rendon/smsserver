package com.inforscience.server;

import android.text.method.ScrollingMovementMethod;
import android.provider.ContactsContract;
import android.provider.Contacts.People;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.ScrollView;
import android.widget.TextView;
import android.provider.Contacts;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.content.Intent;
import android.app.Activity;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.net.Uri;
import android.util.Log;
import android.database.sqlite.SQLiteDatabase;

import java.util.Map;
import java.util.Vector;
import java.io.*;


public class SMSServer extends Activity {
    private HelloServer smsServer;
    private Vector<Contact> contactList;
    private Vector<SentMessage> sentMessageList;
    private Vector<ReceivedMessage> receivedMessageList;

    private static final String COMMAND_GET_CONTACTS = "get_contacts";
    private static final String COMMAND_SEND_MESSAGE = "send_message";
    private static final String COMMAND_GET_LAST_MESSAGE = "get_last_message";

    private static final
    String COMMAND_GET_SENT_MESSAGES = "get_sent_messages";

    private static final
    String COMMAND_GET_RECEIVED_MESSAGES = "get_received_messages";

    private static final
    String COMMAND_CHECK_FOR_NEW_MESSAGE = "check_for_new_message";

    public static final String LOG_OK = "LISTO";
    public static final String LOG_SERVED = "SERVIDO";
    public static final String LOG_FAIL = "FALLO";
    public static final String LOG_WAITING = "ESPERANDO PETICIONES...";

    public static final String STATUS_OK = "OK";
    public static final String STATUS_FAIL = "FAIL";

    public static final String CODE_TRUE = "TRUE";
    public static final String CODE_FALSE = "FALSE";

    public static final String FIELD_SEPARATOR = String.valueOf((char) 31);
    public static final String RECORD_SEPARATOR = String.valueOf((char) 30);

    private BroadcastReceiver intentReceiver;
    ScrollView logScrollView;
    TextView logTextView;


    private boolean newMessage; // True if a new message arrive
    private String lastMessageNumber;
    private String lastMessageText;

    private SQLiteDatabase database;;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        logScrollView = (ScrollView) findViewById(R.id.logScrollView);
        logTextView = (TextView) findViewById(R.id.logTextView);

        logTextView.setMovementMethod(new ScrollingMovementMethod());
        log("Iniciando el servidor...");
        smsServer = new HelloServer();
        try {
            smsServer.start();
            logln(LOG_OK);
        } catch (IOException ioe) {
            logln(LOG_FAIL);
        }

        log("Cargando lista de contactos...");
        Cursor phones = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                null, null, null
        );

        String nc = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME;
        String nuc = ContactsContract.CommonDataKinds.Phone.NUMBER;
        String contacts = "";
        contactList = new Vector<Contact>();
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(nc));
            String phoneNumber = phones.getString(phones.getColumnIndex(nuc));
            contactList.add(new Contact(name, phoneNumber));
        }

        logln(LOG_OK);

        log("Inicializando la base de datos...");
        database = openOrCreateDatabase(DBManager.DB_NAME,
                                        Context.MODE_PRIVATE,
                                        null);
        logln(LOG_OK);

        logln(LOG_WAITING);

        newMessage = false;
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        intentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String msg = intent.getStringExtra("get_msg");

                String[] tokens = msg.split(FIELD_SEPARATOR);
                String number = tokens[0];
                String text = tokens[1];

                newMessage = true;
                lastMessageNumber = number;
                lastMessageText = text;

                logln("NOTIFICACIÓN: Nuevo mensaje");
                logln("De: " + number);
                logln("Mensaje: \"" + text + "\"");
                long id = DBManager
                            .insertReceivedMessage(number, text, database);
            }
        };

        this.registerReceiver(intentReceiver, intentFilter);

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        this.unregisterReceiver(this.intentReceiver);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (database != null) {
            database.close();
        }

        smsServer.stop();
    }


    public String getContacts()
    {
        String ans = "";
        for (int i = 0; i < contactList.size(); i++) {
            String name = contactList.get(i).getName();
            String number = contactList.get(i).getNumber();
            ans += name + FIELD_SEPARATOR + number;
            ans += RECORD_SEPARATOR;
        }

        return ans;
    }

    public String getSentMessages()
    {
        String ans = "";
        Cursor c = DBManager.getAllSentMessages(database);
        if (c.moveToFirst()) {
            do {
                ans += c.getString(1) + FIELD_SEPARATOR;
                ans += c.getString(2);
                ans += RECORD_SEPARATOR;
            } while (c.moveToNext());
        }

        return ans;
    }

    public String getReceivedMessages()
    {
        String ans = "";
        Cursor c = DBManager.getAllReceivedMessages(database);
        if (c.moveToFirst()) {
            do {
                ans += c.getString(1) + FIELD_SEPARATOR;
                ans += c.getString(2);
                ans += RECORD_SEPARATOR;
            } while (c.moveToNext());
        }

        return ans;
    }

    public int sendSMS(final String number, final String text)
    {
        final Activity activity = this;
        activity.runOnUiThread(new Runnable() {
            public void run()
            {
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(number, null, text, null, null);
            }
        });

        return 0; // Success
    }

    // SMS Server
    private class HelloServer extends NanoHTTPD {
        public HelloServer()
        {
            super(8080);
        }

        @Override
        public Response serve(String uri, Method method,
                              Map<String, String> header,
                              Map<String, String> parameters,
                              Map<String, String> files)
        {

            boolean checkForNewMsg = false;
            String answer = "";
            String command = parameters.get("command");
            if (command != null) {
                if (command.equals(COMMAND_GET_CONTACTS)) {
                    log("GET: Lista de contactos...");
                    answer = getContacts();
                    logln(LOG_SERVED);

                } else if (command.equals(COMMAND_SEND_MESSAGE)) {
                    String number = parameters.get("number");
                    String text = parameters.get("text");

                    logln("GET: Enviar mensaje:");
                    logln("Para: " + number);
                    logln("Mensaje: \"" + text + "\"");

                    if (number.matches("[+]?\\d{10,}")) {
                        sendSMS(number, text);
                        logln(LOG_SERVED);

                        DBManager.insertSentMessage(number, text, database);
                        answer = STATUS_OK;
                    } else {
                        logln(LOG_FAIL + " (Verifique el número)");
                        answer = STATUS_FAIL;
                    }

                } else if (command.equals(COMMAND_GET_SENT_MESSAGES)) {
                    log("GET: Mensajes enviados...");
                    answer = getSentMessages();
                    logln(LOG_SERVED);

                } else if (command.equals(COMMAND_GET_RECEIVED_MESSAGES)) {
                    log("GET: Mensajes recibidos...");
                    answer = getReceivedMessages();
                    logln(LOG_SERVED);

                } else if (command.equals(COMMAND_CHECK_FOR_NEW_MESSAGE)) {
                    if (newMessage) {
                        newMessage = false;
                        answer = CODE_TRUE;
                    } else {
                        answer = CODE_FALSE;
                    }

                    checkForNewMsg = true;
                } else if (command.equals(COMMAND_GET_LAST_MESSAGE)) {
                    answer = lastMessageNumber +
                            FIELD_SEPARATOR +
                            lastMessageText;
                }
            }


            if (!checkForNewMsg)
                logln(LOG_WAITING);
            return new NanoHTTPD.Response(answer);
        }

    }

    private class Contact {
        private String name;
        private String number;

        public Contact()
        {
            name = "";
            number = "";
        }

        public Contact(String name, String number)
        {
            this.name = name;
            this.number = number;
        }

        public String getName()
        {
            return name;
        }

        public String getNumber()
        {
            return number;
        }
    }

    private class SentMessage {
        private String receiver;
        private String text;

        public SentMessage()
        {
            receiver = "";
            text = "";
        }

        public SentMessage(String receiver, String text)
        {
            this.receiver = receiver;
            this.text = text;
        }

        public String getReceiver()
        {
            return receiver;
        }

        public String getText()
        {
            return text;
        }
    }


    private class ReceivedMessage {
        private String transmitter;
        private String text;

        public ReceivedMessage()
        {
            transmitter = "";
            text = "";
        }

        public ReceivedMessage(String transmitter, String text)
        {
            this.transmitter = transmitter;
            this.text = text;
        }

        public String getTransmitter()
        {
            return transmitter;
        }

        public String getText()
        {
            return text;
        }
    }


    // ----- Utility methods ---------------------------------------------------

    public void log(final String message)
    {
        Activity activity = this;
        activity.runOnUiThread(new Runnable() {
            public void run()
            {
                logTextView.append(message);
                scrollToBottom();
            }
        });

    }

    public void logln(final String message)
    {
        Activity activity = this;
        activity.runOnUiThread(new Runnable() {
            public void run()
            {
                logTextView.append(message + "\n");
                scrollToBottom();
            }
        });

    }


    private void scrollToBottom()
    {
        logScrollView.post(new Runnable() {
            public void run()
            {
                logScrollView.scrollTo(0, logScrollView.getHeight());
            }
        });


        logScrollView.post(new Runnable() {
            public void run()
            {
                logScrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

}

